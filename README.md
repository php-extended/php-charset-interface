# php-extended/php-charset-interface

A library to list all the available charsets used over the internet taken from the [IANA website](https://www.iana.org/assignments/character-sets/character-sets.xhtml).

![coverage](https://gitlab.com/php-extended/php-charset-interface/badges/master/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install php-extended/php-charset-interface ^8`


## Basic Usage

This library is an interface-only library. 

For a concrete implementation, see [`php-extended/php-charset-object`](https://gitlab.com/php-extended/php-charset-object).


## License

MIT (See [license file](LICENSE)).

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Charset;

use Stringable;

/**
 * CharacterSetReferenceInterface interface file.
 * 
 * This class is a registry for known character sets.
 * 
 * @author Anastaszor
 */
interface CharacterSetReferenceInterface extends Stringable
{
	
	/**
	 * Searches the list of character sets for a character set with the given
	 * name.
	 * 
	 * @param string $name
	 * @return ?CharacterSetInterface
	 */
	public function lookup(string $name) : ?CharacterSetInterface;
	
	/**
	 * Searches the list of character sets for a character set with the given
	 * alias or name.
	 * 
	 * @param string $alias
	 * @return ?CharacterSetInterface
	 */
	public function lookupAlias(string $alias) : ?CharacterSetInterface;
	
}

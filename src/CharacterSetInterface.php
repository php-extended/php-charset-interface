<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-charset-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Charset;

use Stringable;

/**
 * CharacterSetInterface interface file.
 * 
 * This interface represents informations about a given character set.
 * 
 * @author Anastaszor
 */
interface CharacterSetInterface extends Stringable
{
	
	/**
	 * Gets the preferred name of the charset.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the long name of the charset.
	 * 
	 * @return string
	 */
	public function getFullName() : string;
	
	/**
	 * Gets the mib number (for printers).
	 * 
	 * @return integer
	 */
	public function getMIBenum() : int;
	
	/**
	 * Gets a comment about the source, or its name.
	 * 
	 * @return ?string
	 */
	public function getSourceComment() : ?string;
	
	/**
	 * Gets the url of the source, if any.
	 * 
	 * @return ?string
	 */
	public function getSourceUrl() : ?string;
	
	/**
	 * Gets the rfc number in which this character set was proposed, if any.
	 * 
	 * @return ?integer
	 */
	public function getRFCNumber() : ?int;
	
	/**
	 * Gets all the aliases that matches this character set.
	 * 
	 * @return array<integer, string>
	 */
	public function getAliases() : array;
	
}
